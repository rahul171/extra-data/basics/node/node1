const utils = require('@rahul171/utils');
utils.configure({
    colors: true
});

const obj = {
    a: 1,
    b: 'hello',
    c: [1, 2, 3, 4],
    d: ['hi', 'there'],
    e: {
        f: [11, 22, 33, 44],
        g: 'inside'
    }
};

// utils.logC(obj);

const debugP = require('debug');
const debug = debugP('app:a1');
const debug2 = debugP('app:a2');
debugP.enable('app:*');


debug('hello');
debug(obj);
debug2(obj);
// debug(utils.getLine());
debug(obj);
