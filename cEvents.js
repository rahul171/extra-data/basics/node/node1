function EventEmitter() {
    this.events = [];
}

EventEmitter.prototype.emit = function(name, ...args) {
    this.events[name].forEach(fn => {
        fn(...args);
    })
}

EventEmitter.prototype.register = function(name, fn) {
    if (!this.events[name]) {
        this.events[name] = [];
    }
    this.events[name].push(fn);
}

module.exports = EventEmitter;
