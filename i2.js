function Abcd() {
    this.a = 'hello';
    this.greet = function() {
        console.log(this.a);
        this.a = 'hello' + ++this.counter;
    }
    this.counter = 0;
}

module.exports = new Abcd();
