const fs = require('fs');

const readable = fs.createReadStream(`${__dirname}/stream/read.txt`, {
    highWaterMark: 10*1024
});

const writable = fs.createWriteStream(`${__dirname}/stream/write.txt`);

// readable.pipe(writable);

readable.on('data', (chunk) => {
    console.log('read');
    console.log(chunk);
    // console.log(chunk.toString().length);
    writable.write(chunk, (a) => {
        console.log('writable cb');
        console.log(a);
    });
});
