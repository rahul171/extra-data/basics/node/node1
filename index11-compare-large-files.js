const fs = require('fs');

const readable1 = fs.createReadStream(`${__dirname}/stream2/read.txt`, {
    highWaterMark: 10*1024
});

const readable2 = fs.createReadStream(`${__dirname}/stream2/write.txt`, {
    highWaterMark: 10*1024
});

const streamEqual = require('stream-equal');

streamEqual(readable1, readable2, (err, equal) => {
    console.log((date - Date.now())/1000);
    console.log(`equal: ${equal}`);
});

const date = Date.now();
