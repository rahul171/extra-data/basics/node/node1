class A {
    constructor() {
        this.msg = 'Hello';
    }

    methodA() {
        console.log(`msg ${this.msg}`);
    }
}

class B extends A {
    constructor() {
        super();
        this.msg = 'Kello';
    }

    methodB() {
        console.log(`msgB ${this.msg}`);
    }
}
//
let a = new A();
const b = new B();



// without class

function AA() {
    this.msg = 'hello';
}

AA.prototype.methodA = function() {
    console.log(`msg ${this.msg}`);
}

function BB() {
    this.msg = 'kello';
}

BB.prototype.methodB = function() {
    console.log(`msgB ${this.msg}`);
}

const aa = new AA();
const bb = new BB();

for(const key in aa) {
    console.log(`key ${key}`);
    console.log(aa.hasOwnProperty(key));
}

const aaa = new aa.constructor;
const aaaa = new aa.constructor();

// const aaaa = aaa();

const a5 = new AA;

console.log(Object);

const a6 = {};

console.log(aa);

class C extends B {
    constructor(props) {
        super(props);
    }

    methodC() {
        console.log('hello there');
    }
}

const c = new C();

debugger;
