// var a = {
//     b: {},
//     c: 3
// };
//
// var k = a.b;
//
// k = 99;
//
// console.log(k);
// console.log(a);

// var util = require('util');

// function abcd(value, fn) {
//     if (value % 2 === 0) {
//         // fn('ello mate', value+2);
//         throw 'Even Number Exception: Number should not be even';
//     }
//     fn(null, value + 1);
// }

// abcd(44, (err, res) => {
//     if (err) throw 'err: ' + err;
//     console.log('res: ' + res);
// });

// const abcdP = util.promisify(abcd);
//
// abcdP(55)
//     .then(value => {
//         console.log(`value then: ${value}`);
//     })
//     .catch(err => {
//         console.log(`err: ${err}`);
//     });

// var abcdP = util.promisify(abcd);
//
// abcdP('hello').then(value => { console.log('hello: ' + value); }).catch(err => { console.log('error'); console.log(err); });

// function abcd(value, fn) {
//     if (value % 2 === 0) {
//         // fn('ello mate', value+2);
//         throw 'Even Number Exception: Number should not be even';
//     }
//     fn(null, value + 1);
// }
//
// function promisify(fn) {
//     return function(value) {
//         return new Promisify(value, fn);
//     }
// }
//
// function Promisify(value, fn) {
//     this.mainFn = fn;
//     this.runMain = () => {
//         try {
//             fn(value, this.callback);
//         } catch(err) {
//             this.callback(err, value);
//         }
//     }
//     this.callback = (err, value) => {
//         if (err) {
//             this.reject(err);
//         } else {
//             this.resolve(value);
//         }
//     }
// }
//
// Promisify.prototype.then = function(fn) {
//     this.resolve = fn;
//     return this;
//     //this.runMain();
// }
//
// Promisify.prototype.catch = function(fn) {
//     this.reject = fn;
//     this.runMain();
//     return this;
// }
//
// const a = promisify(abcd);
//
// a(56).then(value => {
//     console.log('vall: ' + value);
// }).catch(err => {
//     console.log('errffd: ' + err);
// });

// util.log({a:1,b:{c:()=>{console.log('hello');}, d:'hi'},e:[1,2,3,4,5]});

// import { abcd, val } from './mod.js';
// import * as util from 'util';
//
// abcd();
// console.log(val);
// console.log(util.format("%s %s", 10, 20));
