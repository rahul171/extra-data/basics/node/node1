function Abcd({ text = 'hello' } = {}) {
    this.a = text;
    this.greet = function() {
        this.a = 'hello' + ++this.counter;
        console.log(this.a);
        return this.a;
    }
    this.counter = 0;
}

function AAbcd(params) {
    return new Abcd(params);
}

module.exports = AAbcd;
