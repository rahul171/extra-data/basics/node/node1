const fs = require('fs');
const utils = require('@rahul171/utils');

const readable = fs.createReadStream(`${__dirname}/stream3/read1.txt`, {
    highWaterMark: 4
});

const writable = fs.createWriteStream(`${__dirname}/stream3/write1.txt`);

const through2 = require('through2');

const thTransform = through2(function(chunk, enc, fn) {
    console.log(chunk.toString());
    console.log(fn);
    chunk[2] = '_'.charCodeAt(0);
    console.log(chunk.toString());
    this.push(chunk);
    fn();
});

// readable.pipe(thTransform).pipe(writable); return;

readable.on('data', (chunk) => {
    utils.line({ char: '=' });
    console.log('readable data');
    console.log(chunk.toString());
    thTransform.write(chunk);
    utils.line({ char: '=' });
});

thTransform.on('data', (chunk) => {
    utils.line();
    console.log('thTransform data');
    console.log(chunk.toString());
    writable.write(chunk);
    utils.line();
});

console.log('done!');
