class A {
    constructor() {
        this.a = 'hello A';
    }

    methodA() {
        console.log(`msg A: ${this.a}`);
    }
}

class B extends A {
    constructor() {
        super();
        this.b = 'hi B';
    }

    methodB() {
        console.log(`msg B: ${this.b}`);
    }
}

module.exports = B;
