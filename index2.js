// const EventEmitter = require('./cEvents');
//
// const event = new EventEmitter();
//
// event.register('hello', (value1, value2) => {
//     console.log(`${value1} - ${value2}`);
// });
//
// event.register('hello', (value1, value2) => {
//     console.log(`${value1} + ${value2}`);
// });
//
// event.emit('hello', 'say', 'my');

// const EventEmitter = require('events');
//
// const emitter = new EventEmitter();
//
// emitter.on('abcd', function() {
//     console.log('hello');
// });
//
// emitter.on('abcd', function() {
//     console.log('hello 1');
// });
//
// emitter.emit('abcd');

const util = require('util');
const utils = require('@rahul171/utils');
utils.configure({
    showHidden: true,
    depth: 10,
    colors: true
});

// const a = {
//     b:1,
//     c:2,
//     d:()=>{console.log(`${this.a} : ${this.c}`);}
// };
//
// const b = Object.create(a);
//
// utils.log(b);
//
// function Abcd() {
//     this.a = 'hello';
//     this.b = function() {console.log('hello');}
// }
//
// Abcd.prototype.d = 4;
// Abcd.prototype.elonMusk = () => {
//     console.log('chai sutta');
// };
//
// var aa = new Abcd();

function A() {
    this.a = 'hello';
}

A.prototype.greet = function() {
    console.log(`Hello ${this.a}`);
}

function B() {
    this.b = 'hi';
}

B.prototype.greetings = function() {
    console.log(`Hi ${this.b}`);
}

function inherits(obj, proto) {
    var Fn = function() {
        for (var key in obj) {
            console.log(key);
            Object.defineProperty(this, key, {
                value: obj[key],
            });
        }
    };
    Fn.prototype = proto;
    return new Fn();
}

A.prototype = inherits(A.prototype, B.prototype);

// util.inherits(A, B);

const aa = new A();
const bb = new B();

debugger;
