const fs = require('fs');
const duplexify = require('duplexify');

const readable1 = fs.createReadStream(`${__dirname}/stream3/read1.txt`, {
    highWaterMark: 4
});

const writable1 = fs.createWriteStream(`${__dirname}/stream3/write1.txt`);

const dup = duplexify(writable1, readable1);

const readable2 = fs.createReadStream(`${__dirname}/stream3/read2.txt`, {
    highWaterMark: 4
});

const writable2 = fs.createWriteStream(`${__dirname}/stream3/write2.txt`);

// readable2.pipe(dup).pipe(writable2); return;

readable2.on('data', (chunk) => {
    console.log('readable2 data');
    console.log(chunk.toString());
    dup.write(chunk);
});

dup.on('data', (chunk) => {
    console.log('dup data');
    console.log(chunk.toString());
    writable2.write(chunk);
});

console.log('done!');
