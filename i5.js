const express = require('express');
const app = express();

const server = require('http').createServer(app);
const io = require('socket.io')(server, {
    pingTimeout: 60000,
});

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use((req, res, next) => {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', '*');
    res.set('Access-Control-Allow-Headers', '*');
    res.set('Access-Control-Allow-Credentials', true);
    next();
});

app.get('/', (req, res, next) => {
    var a = require('./i3')();
    res.send(a.greet());
});

server.listen(port = 5000, () => {
    console.log(`server started on ${port}`);
});
