const http = require('http');
const fs = require('fs');

http.createServer((req, res) => {
    res.writeHead(200, {
        // 'Content-Type': 'text/html'
        'Content-Type': 'application/json'
    });
    // fs.createReadStream(`${__dirname}/public/index.html`).pipe(res);
    fs.createReadStream(`${__dirname}/public/config.json`).pipe(res);
}).listen(1337, '127.0.0.1', (data) => {
    console.log('listening');
});
