const fs = require('fs');

const readable = fs.createReadStream(`${__dirname}/stream3/read1.txt`, {
    highWaterMark: 4
});

const writable = fs.createWriteStream(`${__dirname}/stream3/write1.txt`);

const quote = require('quote-stream')();

// readable.pipe(quote).pipe(writable); return;

readable.on('data', (chunk) => {
    console.log('readable data');
    console.log(chunk.toString());
    quote.write(chunk);
});

quote.on('data', (chunk) => {
    console.log('quote data');
    console.log(chunk.toString());
    writable.write(chunk);
});

console.log('done!');
