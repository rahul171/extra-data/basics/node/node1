const fs = require('fs');
const zlib = require('zlib');

const readable = fs.createReadStream(`${__dirname}/stream2/read.txt`, {
    highWaterMark: 10*1024
});

const writable = fs.createWriteStream(`${__dirname}/stream2/compressed.txt.gz`);

const gzip = zlib.createGzip();

// readable.pipe(gzip).pipe(writable); return;

console.log('start');

gzip.on('data', (chunk) => {
    console.log(`compressed data coming`);
    console.log(chunk.toString());
    writable.write(chunk);
});

gzip.write('something');
gzip.write('something else');
gzip.write('something else else ');
gzip.write('something 2');
gzip.write('something 3');

console.log('middleware');

// readable.on('data', (chunk) => {
//     console.log('data coming');
//     gzip.write(chunk);
// });

console.log('end!');
