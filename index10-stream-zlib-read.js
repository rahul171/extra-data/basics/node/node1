const fs = require('fs');
const zlib = require('zlib');

const readable = fs.createReadStream(`${__dirname}/stream2/compressed.txt.gz`, {
    highWaterMark: 10*1024
});

const writable = fs.createWriteStream(`${__dirname}/stream2/write.txt`);

const gzip = zlib.createGunzip();

readable.pipe(gzip).pipe(writable); return;

console.log('start');

gzip.on('data', (chunk) => {
    console.log('uncompressed data coming');
    writable.write(chunk);
});

console.log('middleware');

readable.on('data', (chunk) => {
    console.log('data coming');
    gzip.write(chunk);
});

console.log('end!');
